package com.bankapp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankapp.model.service.AccountService;
import com.bankapp.web.beans.DepositBeans;
import com.bankapp.web.beans.TransferBean;
import com.bankapp.web.beans.WithdrawBean;

//......../mgr/transfer
@RestController
@RequestMapping(path = "mgr")
public class TransactionController {
	private AccountService accountService;

	@Autowired
	public TransactionController(AccountService accountService) {
		this.accountService = accountService;
	}

	@PostMapping(path = "transfer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String transferFund(@RequestBody TransferBean bean) {
		accountService.transfer(bean.getFromAccountId(), bean.getToAccountId(), bean.getAmount());
		return "fund is transferred";
	}
	@PostMapping(path = "deposit", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String depositFund(@RequestBody DepositBeans bean) {
		accountService.deposit(bean.getAccountId(),bean.getAmount());
		return "amount deposited successfully";
	}
	@PostMapping(path = "withdraw", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String withdrawFund(@RequestBody WithdrawBean bean) {
		accountService.withdraw(bean.getAccountId(),bean.getAmount());
		return "amount withdrawl successfully";
	}
	
}
